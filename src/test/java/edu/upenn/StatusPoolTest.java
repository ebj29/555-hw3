package edu.upenn;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis455.mapreduce.master.WorkerPool;

public class StatusPoolTest {

    private static WorkerPool statusPool;

    @BeforeClass
    public static void setup() {
        statusPool = WorkerPool.instance();
    }

    @Test
    public void test() {
        statusPool.put("0.0.0.0", null);
        assertEquals(1, statusPool.workers().size());
    }
    
}

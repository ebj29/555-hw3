package edu.upenn.cis455.mapreduce.worker;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorkerStatusManager implements Runnable {
    private final Logger logger = LogManager.getLogger(WorkerStatusManager.class);
    private AtomicBoolean running;
    private String masterAddress;
    private int port;
    private String id = UUID.randomUUID().toString();
    private static WorkerStatusManager instance;


    public static WorkerStatusManager instance() {
        return instance;
    }

    public static WorkerStatusManager instance(String masterAddress, int port) {
        if (instance == null) instance = new WorkerStatusManager(masterAddress, port);
        return instance;
    }

    private WorkerStatusManager(String masterAddress, int port) {
        this.masterAddress = masterAddress;
        this.port = port;
        running = new AtomicBoolean(true);
    }

    public void stop() {
        logger.info(id + " - stopping worker status manager.");
        running.set(false);
    }

    @Override
    public void run() {
        while (running.get()) {
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                
            }
            logger.info(id + " - sending status message.");
            sendStatusMessage();
        }
    }

    private void sendStatusMessage() {
        try {
            URL url = new URL("http://" + masterAddress + "/workerstatus?port=" + port +"&status=IDLE&job=test&keysRead=42&keysWritten=37");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.getResponseCode();
            conn.disconnect();
        } catch (IOException e) {

        }
        logger.info(id + " - message sent.");
    }
    
}

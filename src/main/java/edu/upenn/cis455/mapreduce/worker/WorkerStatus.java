package edu.upenn.cis455.mapreduce.worker;

import edu.upenn.cis.stormlite.TopologyContext;

public class WorkerStatus {
    private int port;
    private TopologyContext.STATE status;
    private String job;
    private int keysRead;
    private int keysWritten;

    public int port() {
        return port;
    }

    public WorkerStatus withPort(int port) {
        this.port = port;
        return this;
    }

    public TopologyContext.STATE status() {
        return status;
    }

    public WorkerStatus withStatus(TopologyContext.STATE status) {
        this.status = status;
        return this;
    }

    public String job() {
        return job;
    }

    public WorkerStatus withJob(String job) {
        this.job = job;
        return this;
    }

    public int keysRead() {
        return keysRead;
    }

    public WorkerStatus withKeysRead(int keysRead) {
        this.keysRead = keysRead;
        return this;
    }
    
    public int keysWritten() {
        return keysWritten;
    }

    public WorkerStatus withKeysWritten(int keysWritten) {
        this.keysWritten = keysWritten;
        return this;
    }

}

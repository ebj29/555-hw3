package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import edu.upenn.cis455.mapreduce.master.handlers.ShutdownHandler;
import edu.upenn.cis455.mapreduce.master.handlers.StatusPageHandler;
import edu.upenn.cis455.mapreduce.master.handlers.SubmitJobHandler;
import edu.upenn.cis455.mapreduce.master.handlers.WorkerStatusHandler;

public class MasterServer {  
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");

            return ("<html><head><title>Master</title></head>\n" +
                    "<body>Hi, I am the master!</body></html>");
        });

    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        new Thread(StatusManager.instance()).start();

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here
        get("/status", new StatusPageHandler());
        get("/workerstatus", new WorkerStatusHandler());
        get("/shutdown", new ShutdownHandler());
        post("/submitjob", new SubmitJobHandler());

        // TODO: route handler for /workerstatus reports from the workers
    }
}


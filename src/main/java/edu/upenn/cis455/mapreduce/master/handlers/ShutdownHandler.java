package edu.upenn.cis455.mapreduce.master.handlers;

import edu.upenn.cis455.mapreduce.master.WorkerPool;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class ShutdownHandler implements Route {

    @Override
    public Object handle(Request request, Response response) throws Exception {
        Spark.stop();
        WorkerPool.instance().stop();
        return "";
    }
    
}

package edu.upenn.cis455.mapreduce.master;

public class StatusManager implements Runnable {
    private boolean running;
    private static StatusManager instance;

    public static StatusManager instance() {
        if (instance == null) instance = new StatusManager();
        return instance;
    }

    protected StatusManager() {
        running = true;
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {

            }
            WorkerPool.instance().clean();
        }
        
    }

    public void stop() {
        running = false;
    }
}
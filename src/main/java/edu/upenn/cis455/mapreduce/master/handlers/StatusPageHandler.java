package edu.upenn.cis455.mapreduce.master.handlers;

import edu.upenn.cis455.mapreduce.master.WorkerPool;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;
import spark.Request;
import spark.Response;
import spark.Route;

public class StatusPageHandler implements Route {

    public static final String MSG = 
        "<html>" +
        "<head><title>CIS 455 HW3: Master</title></head>" + 
        "<body>" + 
            "<h1>CIS 455 HW3: Master</h1>" +
            "<div>" +
                "<h4> Created by: Eric Jackson (ebj29) </h4>" +
            "</div>" + 
            "<div>" +
                "<h3>Submit a New Job</h3>" +
                "<form method=\"POST\" action=\"/submitjob\" id=\"job\">" +
                    "Job Name: <input type=\"text\" name=\"jobname\"/><br/>" +
                    "Class Name: <input type=\"text\" name=\"classame\"/><br/>" +
                    "Input Directory: <input type=\"text\" name=\"input\"/><br/>" +
                    "Output Directory: <input type=\"text\" name=\"output\"/><br/>" +
                    "Map Threads: <input type=\"text\" name=\"map\"/><br/>" + 
                    "Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/>" +
                "</form>" +
                "<button type =\"submit\" form=\"job\" value =\"Submit\">Submit</button>" +
            "</div>" +
            "<div>" +
                "<h3>Worker Status</h3>" +
                "<div>" +
                "{WORKERS}" +
                "</div>" + 
            "</div>" + 
        "</body>" +
        "</html>";

    private static String WORKER_MSG = 
        "<p>" +
            "{NUM}: " +
            "port={PORT}, "+
            "status={STATUS}, " +
            "job={JOB}, " +
            "mapOutputs={MAP}, "+
            "reduceOutputs={REDUCE} " +
            "results=[]" +
        "</p>";

    private WorkerPool statusPool;

    public StatusPageHandler() {
        statusPool = WorkerPool.instance();
    }

    @Override
    public Object handle(Request req, Response res) throws Exception {
        res.type("text/html");
        return MSG.replace("{WORKERS}", workersContent());
    }

    private String workersContent() {
        StringBuilder content = new StringBuilder();
        int i = 0;
        for (WorkerStatus worker : statusPool.workers()) {
            String line = WORKER_MSG
                .replace("{NUM}", String.valueOf(i))
                .replace("{PORT}", String.valueOf(worker.port()))
                .replace("{STATUS}", worker.status().toString())
                .replace("{JOB}", worker.job())
                .replace("{MAP}", "???")
                .replace("{REDUCE}", "???");
            content.append(line);
            i++;
        }
        return content.toString();
    }
    
}

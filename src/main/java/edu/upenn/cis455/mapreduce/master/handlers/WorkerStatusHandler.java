package edu.upenn.cis455.mapreduce.master.handlers;

import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis455.mapreduce.master.WorkerPool;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class WorkerStatusHandler implements Route {
    
    private WorkerPool statusPool;

    public WorkerStatusHandler() {
        statusPool = WorkerPool.instance();
    }

    @Override
    public Object handle(Request req, Response res) throws Exception {
        WorkerStatus worker = new WorkerStatus()
            .withPort(Integer.valueOf(req.queryParams("port")))
            .withStatus(statusfromString(req.queryParams("status")))
            .withJob(req.queryParams("job"))
            .withKeysRead(Integer.valueOf(req.queryParams("keysRead")))
            .withKeysWritten(Integer.valueOf(req.queryParams("keysWritten")));
            statusPool.put(req.ip(), worker);
        res.redirect("/status", 200);
        return "";
    }

    private TopologyContext.STATE statusfromString(String str) {
        switch (str) {
            case "INIT":
                return TopologyContext.STATE.INIT;
            case "MAPPING":
                return TopologyContext.STATE.MAPPING;
            case "REDUCING":
                return TopologyContext.STATE.REDUCING;
            case "IDLE":
                return TopologyContext.STATE.IDLE;
            default:
                Spark.halt(400);
                return null;
        }
    }
    
}
